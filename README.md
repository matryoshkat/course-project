# Training db project
(second year of university)
### Used: 
1. SQL 
2. DBMS:SQLite 
3. WPF

## Preview:

### As (new) client :

![](https://github.com/matryosha/Training-db-project/blob/preview-files/training%20db%20project%201%20gif.gif)

### As admin :

![](https://github.com/matryosha/Training-db-project/blob/preview-files/training%20db%20project%202%20gif.gif)

### DB structure:

![](https://github.com/matryosha/Training-db-project/blob/preview-files/training%20db%20project%203%20gif.gif)



﻿using System.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using CourseProject.ViewModels;

namespace CourseProject.Views
{
    /// <summary>
    /// Interaction logic for AdminFilmsView.xaml
    /// </summary>
    public partial class AdminFilmsView : UserControl
    {

        private LastColumn lastColumn = new LastColumn();

        public AdminFilmsView()
        {
            InitializeComponent();
        }

        private void Header_click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AdminFilmsViewModel;
            var column = sender as GridViewColumnHeader;
            var tag = column.Tag.ToString();

            lastColumn.Direction = lastColumn.Column == column && lastColumn.Direction == "desc" ? "asc" : "desc";
            context.Films.Sort = $"{tag} {lastColumn.Direction}";
            lastColumn.Column = column;
        }

        private class LastColumn
        {
            public string Direction { get; set; } = "desc";
            public GridViewColumnHeader Column { get; set; }

        }

        private void DeleteFilm_UI(object sender, RoutedEventArgs e)
        {
            if(FilmListView.SelectedItem == null)
                return;
            var contex = DataContext as AdminFilmsViewModel;
            var row = FilmListView.SelectedItem as DataRowView;
            if(!string.IsNullOrEmpty(row.Row.ItemArray[3].ToString())) return;
            contex.DeleteFilm(row.Row.ItemArray[0].ToString(), row.Row.ItemArray[1].ToString(), decimal.Parse(row.Row.ItemArray[2].ToString()).ToString(new CultureInfo("en-US")));
        }
    }
}

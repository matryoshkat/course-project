﻿using System;
using System.Windows;
using System.Windows.Controls;
using CourseProject.Properties;
using CourseProject.ViewModels;
using Microsoft.Win32;

namespace CourseProject.Views
{
    public partial class AdminControlView : UserControl
    {
        public AdminControlView()
        {
            InitializeComponent();
            TitleTextBlock.Text = "Клієнти";
        }

        private void OpenFilms_UI(object sender, RoutedEventArgs e)
        {
            MenuToggleButton.IsChecked = false;
            var context = DataContext as AdminControlViewModel;
            context.OpenFilmsView();
            TitleTextBlock.Text = "Фiльми";
        }

        private void OpenClients_UI(object sender, RoutedEventArgs e)
        {
            MenuToggleButton.IsChecked = false;
            var context = DataContext as AdminControlViewModel;
            context.OpenClientsView();
            TitleTextBlock.Text = "Клієнти";

        }

        private void OpenGenres_UI(object sender, RoutedEventArgs e)
        {
            MenuToggleButton.IsChecked = false;
            var context = DataContext as AdminControlViewModel;
            context.OpenGenresView();
            TitleTextBlock.Text = "Жанри";
        }

        private void ChangePathtoDb(object sender, RoutedEventArgs e)
        {
            string pathToFile = String.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                pathToFile = openFileDialog.FileName;
            Settings.Default.ConnectionString = $"Data Source={pathToFile};Version=3;";

            var context = DataContext as AdminControlViewModel;
            context.Reset();
            TitleTextBlock.Text = "Клієнти";
        }
    }
}

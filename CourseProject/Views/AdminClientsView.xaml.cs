﻿using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CourseProject.ViewModels;

namespace CourseProject.Views
{
    /// <summary>
    /// Interaction logic for AdminClientsView.xaml
    /// </summary>
    public partial class AdminClientsView : UserControl
    {
        private LastColumn lastColumn = new LastColumn();
        private DataRowView lastSelected;

        public AdminClientsView()
        {
            InitializeComponent();
//            RentFilmsListView.PreviewMouseLeftButtonDown += (MouseButtonEventHandler)(
//                (o, e) => {
//                    ListViewItem lbItem = FindAncestor<ListViewItem>(e.OriginalSource as DependencyObject) as ListViewItem;
//                    bool isListViewItem = lbItem != null;
//                    bool isButtonClicked = FindAncestor<Button>(e.OriginalSource as DependencyObject) is Button;
//                    if (isListViewItem)
//                    {
//                        if (isButtonClicked)
//                            lbItem.SetValue(ListBoxItem.IsSelectedProperty, true);
//                        else e.Handled = true; //Handled means will not pass to ListView for selection.
//                    }
//                });
        }

        public static T FindAncestor<T>(DependencyObject obj) where T : DependencyObject
        {
            while (obj != null)
            {
                T o = obj as T;
                if (o != null)
                    return o;
                obj = VisualTreeHelper.GetParent(obj);
            }
            return default(T);
        }

        private void Header_click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as AdminClientsViewModel;
            var column = sender as GridViewColumnHeader;
            var tag = column.Tag.ToString();

            lastColumn.Direction = lastColumn.Column == column && lastColumn.Direction == "desc" ? "asc" : "desc";
            context.Clients.Sort = $"{tag} {lastColumn.Direction}";
            lastColumn.Column = column;
        }

        private class LastColumn
        {
            public string Direction { get; set; } = "desc";
            public GridViewColumnHeader Column { get; set; }

        }

        private void EditBtn_UI(object sender, RoutedEventArgs e)
        {
            if (ClientsListView.SelectedItem == null) return;
            var contex = DataContext as AdminClientsViewModel;
            contex.EditClient((ClientsListView.SelectedItem as DataRowView).Row.ItemArray[2].ToString());

        }

        private void ClientsListView_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(ClientsListView.SelectedItem == null) return;
            if(ClientsListView.SelectedItem == lastSelected) return;
            lastSelected = (DataRowView) ClientsListView.SelectedItem ;

            var contex = DataContext as AdminClientsViewModel;
            contex.UpdateRentFilm((ClientsListView.SelectedItem as DataRowView).Row.ItemArray[2].ToString());
        }

        private void delete_film(object sender, RoutedEventArgs e) {
            if (RentFilmsListView.SelectedItem == null) return;

            var contex = DataContext as AdminClientsViewModel;

            contex.ReturnFilm(RentFilmsListView.SelectedItem);
        }
    }
}

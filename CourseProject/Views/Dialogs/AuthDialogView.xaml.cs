﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace CourseProject.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AuthDialogView.xaml
    /// </summary>
    public partial class AuthDialogView : UserControl
    {
        public AuthDialogView()
        {
            InitializeComponent();
        }

        private void PhoneNumber_restriction(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}

﻿using System.Windows;
using System.Windows.Controls;
using CourseProject.ViewModels.Dialogs;

namespace CourseProject.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditClientDialogView.xaml
    /// </summary>
    public partial class EditClientDialogView : UserControl
    {
        public EditClientDialogView()
        {
            InitializeComponent();
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            var context = DataContext as EditClientDialogViewModel;
            context.CloseAction();
        }
    }
}

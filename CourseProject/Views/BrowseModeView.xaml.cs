﻿using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CourseProject.Helpers.Base;
using CourseProject.ViewModels;

namespace CourseProject.Views
{
    public partial class BrowseModeView : UserControl
    {

        private LastColumn lastColumn = new LastColumn();
        private List<Film> _orderedFilmsList;

        public BrowseModeView()
        {
            InitializeComponent();
        }

        private void Header_click(object sender, RoutedEventArgs e)
        {
            var context = DataContext as BrowseModeViewModel;
            var column = sender as GridViewColumnHeader;
            var tag = column.Tag.ToString();

            lastColumn.Direction = lastColumn.Column == column && lastColumn.Direction == "desc" ? "asc" : "desc";
            context.Films.Sort = $"{tag} {lastColumn.Direction}";
            lastColumn.Column = column;
        }

        private class LastColumn
        {
            public string Direction { get; set; } = "desc";
            public GridViewColumnHeader Column { get; set; }

        }

        private void BuyBtn(object sender, MouseButtonEventArgs e)
        {
            if(filmListView.SelectedItems.Count == 0) return;
            KostilToggle.IsChecked = true;
        }

        private void ConfirmOrderBtn(object sender, MouseButtonEventArgs e)
        {
            //I feel sorry about this (个_个)
            //Should do smth like this:
            //https://stackoverflow.com/questions/24665660/convert-datarow-to-object-with-automapper

            var context = DataContext as BrowseModeViewModel;
            var filmsRaw = filmListView.SelectedItems;
            _orderedFilmsList = new List<Film>();
            foreach (var filmRaw in filmsRaw)
            {
                var itemArr = (filmRaw as DataRowView).Row.ItemArray;

                var film = new Film();
                film.Name = itemArr[0].ToString();
                film.Year = itemArr[1].ToString();
                film.Rating = double.Parse(itemArr[2].ToString());
                film.Genres = itemArr[3].ToString();

                _orderedFilmsList.Add(film);
            }

            context.TakeOrder(_orderedFilmsList);
            KostilToggle.IsChecked = false;
        }

        private void RatingRestriction(object sender, TextCompositionEventArgs e)
        {
                Regex regex = new Regex("[^0-9,]+");
                e.Handled = regex.IsMatch(e.Text);
        }

        private void LogoutBtn(object sender, MouseButtonEventArgs e)
        {
            var context = DataContext as BrowseModeViewModel;
            context.Logout();
        }
    }
}

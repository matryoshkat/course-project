﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.Properties;
using CourseProject.ViewModels.Dialogs;
using MaterialDesignThemes.Wpf;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class BrowseModeViewModel :
        UserControlViewModel, INotifyPropertyChanged
    {

        private DataView _films;
        private int _userId;
        private MvxSubscriptionToken _loginToken;
        public DataView Films
        {
            get
            {
                if (_films == null)
                {
                    _films = GetFilmsFromDb();
                }

                return _films;

            }
            set
            {
                _films = value;
                OnPropertyChanged("Films");
            }
        }

        private DataView GetFilmsFromDb()
        {
            try
            {
                DataTable dataTable = new DataTable();
                using (SQLiteConnection connection =
                    new SQLiteConnection(Settings.Default.ConnectionString))
                {
                    connection.Open();
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter();
                    adapter.SelectCommand =
                        new SQLiteCommand("select name, year, rating, id from film where available='true'",
                            connection);
                    adapter.Fill(dataTable);

                    //Filling film genres
                    dataTable.Columns.Add("genre");

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        
                        var sql =
                            $"SELECT g1.name AS genre FROM genre g1" +
                            $" JOIN film_genre fg1 " +
                            $"ON g1.id==fg1.genre_id " +
                            $"JOIN film f1 " +
                            $"ON f1.id==fg1.film_id " +
                            $"WHERE f1.id=={int.Parse(dataTable.Rows[i]["id"].ToString())}";

                        SQLiteCommand command = new SQLiteCommand(sql, connection);
                        SQLiteDataReader reader = command.ExecuteReader();

                        reader.Read();
                        string result = reader["genre"].ToString();

                        while (reader.Read())
                            result += ", " + reader["genre"];

                        dataTable.Rows[i]["genre"] = result;
                    }

                    connection.Close();
                    _films = dataTable.DefaultView;
                    return _films;
                }
            }


            catch (SQLiteException e)
            {
                Settings.Default.IsFirstLaunch = true;
                Console.WriteLine(e.Message);
                App.Current.MainWindow.Close();
                throw null;
            }
        }

        private string _searchDateBefore;
        public string SearchDateBefore
        {
            get => _searchDateBefore;
            set
            {
                _searchDateBefore = value;
                OnPropertyChanged("SearchDateBefore");
            }
        }

        private string _searchDateAfter;
        public string SearchDateAfter
        {
            get => _searchDateAfter;
            set
            {
                _searchDateAfter = value;
                OnPropertyChanged("SearchDateAfter");
            }
        }

        private string _searchNameString;
        public string SearchNameString
        {
            get => _searchNameString;
            set
            {
                _searchNameString = value;
                OnPropertyChanged("SearchNameString");
            }
        }   

        private string _searchRatingString;
        public string SearchRatingString
        {
            get => _searchRatingString;
            set
            {
                _searchRatingString = value;
                OnPropertyChanged("SearchRatingString");
            }
        }

        private RelayCommand _openAccountCommand;

        public RelayCommand OpenAccountCommand =>
            _openAccountCommand ?? (_openAccountCommand = new RelayCommand(async o =>
            {
                if (_userId == 0)
                {
                    var authDialog = new AuthDialogViewModel(_messenger, dir.ToAccount);
                    await DialogHost.Show(authDialog, "MainDialogHost", delegate (object sender, DialogOpenedEventArgs args)
                    {
                        authDialog.CloseAction = args.Session.Close;
                    });
                }
                else
                {
                    _messenger.Publish(new OpenAccountMessage(this, _userId));
                }
                
                /*if (_userId == 0)
                {
                    _messenger.Publish(new OpenAuthAndRegMessage(this, dir.ToAccount));
                    return;
                }
                _messenger.Publish(new OpenAccountMessage(this, _userId));*/

            }));

        private RelayCommand _openGenresSelectorCommand;
        public RelayCommand OpenGenresSelectorCommand =>
            _openGenresSelectorCommand ?? (_openGenresSelectorCommand = new RelayCommand(async o =>
                {
                    var genresSelectVm = new GenresSelectDialogViewModel(_messenger, SearchGenreList);
                    await DialogHost.Show(genresSelectVm, "MainDialogHost", delegate (object sender, DialogOpenedEventArgs args)
                    {
                        genresSelectVm.CloseAction = args.Session.Close;
                    });
                    OnPropertyChanged("SearchGenreList");
                }));

        private RelayCommand _clearSearchFieldsCommand;
        public RelayCommand ClearSearchFields =>
            _clearSearchFieldsCommand ?? (_clearSearchFieldsCommand = new RelayCommand(
                o =>
                {

                    foreach (var genre in SearchGenreList)
                    {
                        genre.IsSelected = false;
                    }

                    var newList = SearchGenreList;
                    SearchGenreList = newList;
                    OnPropertyChanged("SearchGenreList");
                    SearchNameString = String.Empty;
                    SearchRatingString = String.Empty;
                    SearchDateAfter = String.Empty;
                    SearchDateBefore = String.Empty;
                    Films.RowFilter = String.Empty;
                }));


        public void TryFindFilms()
        {
            try
            {
                string query = $"name LIKE '*{SearchNameString}*' ";
                if (!String.IsNullOrEmpty(SearchDateAfter))
                    query += $"AND year > '{SearchDateAfter}' ";
                if (!String.IsNullOrEmpty(SearchDateBefore))
                    query += $"AND year < '{SearchDateBefore}' ";
                if (!String.IsNullOrEmpty(SearchRatingString))
                    query += $"AND rating > '{Decimal.Parse(SearchRatingString)}' ";
                foreach (var genre in SearchGenreList)
                {
                    if (genre.IsSelected)
                        query += $"AND genre LIKE '*{genre.Name}*' ";
                }
                Films.RowFilter = query;
            }
            catch (Exception e)
            {

            }
            
        }

        private ObservableCollection<Genre> _searchGenreList;
        public ObservableCollection<Genre> SearchGenreList
        {
            get => _searchGenreList ?? (_searchGenreList = new ObservableCollection<Genre>());
            set
            {
                _searchGenreList = value;
                OnPropertyChanged("SearchGenreList");
            }
        }

        public BrowseModeViewModel(IMvxMessenger messenger, int userId) : base(messenger)
        {
            FillGenreList();
            _loginToken = _messenger.Subscribe<LoginMessage>(SetUserId);
            _userId = userId;
        }

        private void SetUserId(LoginMessage message)
        {
            _userId = message.UserId;
            CheckUserIdPreconditional(_userId);
        }

        public void Logout()
        {
            _messenger.Publish(new OpenLoginViewMessage(this));
        }

        public async void TakeOrder(List<Film> films)
        {
            //Open Auth dialog if there's no login user
            if (_userId == 0)
            {
                var authDialog = new AuthDialogViewModel(_messenger);
                await DialogHost.Show(authDialog, "MainDialogHost", delegate (object sender, DialogOpenedEventArgs args)
                {
                    authDialog.CloseAction = args.Session.Close;
                });
            }

            if (_userId != 0)
            {
                var confirmOrderVM = new ConfirmOrderDialogViewModel(_messenger, _userId, films);
                 await DialogHost.Show(confirmOrderVM, "MainDialogHost", delegate (object sender, DialogOpenedEventArgs args)
                {
                    confirmOrderVM.CloseAction = args.Session.Close;
                });

                Films = GetFilmsFromDb();
            }

        }

        private void FillGenreList()
        {
            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                var sql = "select * from genre";
                var command = new SQLiteCommand(sql, connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var genre = new Genre {Name = reader["name"].ToString(), IsSelected = false};
                    SearchGenreList.Add(genre);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if(propertyName == "SearchNameString" || propertyName == "SearchDateAfter" ||
               propertyName == "SearchDateBefore" || propertyName == "SearchRatingString" ||
               propertyName == "SearchGenreList")
                TryFindFilms();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [Conditional("DEBUG")]
        private static void CheckUserIdPreconditional(int id)
        {
            Console.WriteLine($@"User ID is {id} now");
        }
    }

    class Genre : INotifyPropertyChanged
    {
        public int Id { get; set; }

        public string Name { get; set; }

        private bool _isSelected;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

       
}

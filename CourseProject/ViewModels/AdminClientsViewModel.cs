﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CourseProject.Helpers;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.Properties;
using CourseProject.ViewModels.Dialogs;
using MaterialDesignThemes.Wpf;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class AdminClientsViewModel : UserControlViewModel, INotifyPropertyChanged
    {

        private MvxSubscriptionToken _updateClientsToken;
        private MvxSubscriptionToken _clientSearchToken;
        private MvxSubscriptionToken _filmSearchToken;
        private int _currentUserId;
        private string _currentLogin;
        private DataView _rentFilms;
        public DataView RentFilms
        {
            get => _rentFilms;
            set
            {
                _rentFilms = value;
                OnPropertyChanged("RentFilms");
            }
        }


        private DataView _clients;
        public DataView Clients
        {
            get
            {
                if (_clients != null) return _clients;
                _clients = GetClientFromDb();
                return _clients;
            }
            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
            }
        }

        private DataView GetClientFromDb()
        {
            DataTable dataTable = new DataTable();
            if(Settings.Default.ConnectionString == "Data Source=;Version=3;")
                PathToDb.ChangePathToDb();
            using (SQLiteConnection connection =
                new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter();
                adapter.SelectCommand =
                    new SQLiteCommand(@"
                                          SELECT 
                                               name,
                                               phone,
                                               login,
                                               count(cf.film_id) AS count
                                          FROM client AS c
                                               LEFT JOIN
                                               client_film cf ON c.id = cf.client_id
                                         GROUP BY c.id",
                        connection);
                adapter.Fill(dataTable);

                connection.Close();
                _clients = dataTable.DefaultView;
                return _clients;
            }
        }

        private RelayCommand _searchFilmCommand;

        public RelayCommand SearchFilmCommand =>
            _searchFilmCommand ?? (_searchFilmCommand = new RelayCommand( async o =>
            {
                var filmSearchDialog = new FilmSearchDialogViewModel(_messenger);
                await DialogHost.Show(filmSearchDialog,
                    delegate (object sender, DialogOpenedEventArgs args)
                    {
                        filmSearchDialog.CloseAction = args.Session.Close;
                    });
            }));

        private RelayCommand _searchClientCommand;

        public RelayCommand SearchClientCommand =>
            _searchClientCommand ?? (_searchClientCommand = new RelayCommand( async o =>
            {
                var clientSearchVm = new ClientSearchDialogViewModel(_messenger);
                await DialogHost.Show(clientSearchVm,
                    delegate (object sender, DialogOpenedEventArgs args)
                    {
                        clientSearchVm.CloseAction = args.Session.Close;
                    });
            }));

        public void ReturnFilm(object item) {
            if (item == null) throw new ArgumentNullException();
            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString)) {
                connection.Open();

                var filmName = (item as DataRowView).Row.ItemArray[0].ToString();
                var filmYear = (item as DataRowView).Row.ItemArray[1].ToString();

                var sql = $"update film SET available='true' WHERE name='{filmName}' AND year='{filmYear}'";
                ExecuteNonQuery(sql, connection);

                sql = $"select id from film WHERE name='{filmName}' AND year='{filmYear}'";

                var selectIdadapter = new SQLiteDataAdapter();
                selectIdadapter.SelectCommand = new SQLiteCommand(sql, connection);
                var dt = new DataTable();
                selectIdadapter.Fill(dt);
                var filmId = int.Parse(dt.Rows[0].ItemArray[0].ToString());

                /*                var command = new SQLiteCommand(sql, connection);
                                var reader = command.ExecuteReader();
                                reader.Read();
                                var filmId = int.Parse(reader["id"].ToString());*/

                ExecuteNonQuery($"delete from client_film where client_id={_currentUserId} AND film_id={filmId}", connection);


                connection.Close();

                UpdateRentFilm(_currentLogin);
                _messenger.Publish(new UpdateClientsMessage(this));
            }
        }


        private void ExecuteNonQuery(string query, SQLiteConnection connection)
        {
            var command = new SQLiteCommand(query, connection);
            command.ExecuteNonQuery();
        }

        public void UpdateRentFilm(string login)
        {
            _currentLogin = login;
            DataTable dataTable = new DataTable();
            using (SQLiteConnection connection =
                new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();




                var query = $"select id from client where login='{login}'";
                var command = new SQLiteCommand(query, connection);
                var reader = command.ExecuteReader();
                reader.Read();
                var client_id = int.Parse(reader["id"].ToString());

                _currentUserId = client_id;

                SQLiteDataAdapter adapter = new SQLiteDataAdapter();
                adapter.SelectCommand =
                    new SQLiteCommand($@"
                                          SELECT 
                                               film.name,
                                               film.year,
                                               client_film.rent_start_date,
                                               client_film.rent_end_date
                                          FROM client_film
                                               JOIN
                                               film ON client_film.film_id = film.id
                                         WHERE client_id = {client_id};",
                        connection);
                adapter.Fill(dataTable);

                connection.Close();
                RentFilms = dataTable.DefaultView;
            }
        }

        public async void EditClient(string login)
        {
            var editClientDialogVm = new EditClientDialogViewModel(_messenger, login);
            await DialogHost.Show(editClientDialogVm,
                delegate(object sender, DialogOpenedEventArgs args)
                {
                    editClientDialogVm.CloseAction = args.Session.Close;
                });
        }

        public AdminClientsViewModel(IMvxMessenger messenger) : base(messenger)
        {
            _updateClientsToken = _messenger.Subscribe<UpdateClientsMessage>(UpdateClients);
            _clientSearchToken = _messenger.Subscribe<SearchClientMessage>(ClientSearch);
            _filmSearchToken = _messenger.Subscribe<SearchFilmMessage>(FilmSearch);
        }

        private void FilmSearch(SearchFilmMessage message)
        {
            if (string.IsNullOrEmpty(message.FilmName))
            {
                RentFilms.RowFilter = "";
                return;
            }

            RentFilms.RowFilter = $"name LIKE '*{message.FilmName}*'";
        }

        private void ClientSearch(SearchClientMessage message)
        {
            if (message.Phone == string.Empty)
            {
                Clients.RowFilter = "";
                return;
            }
            Clients.RowFilter = $"phone LIKE '{message.Phone}'";
        }

        private void UpdateClients(MvxMessage message)
        {
            Clients = GetClientFromDb();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

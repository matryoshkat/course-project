﻿using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Properties;
using CourseProject.ViewModels.Dialogs;
using MaterialDesignThemes.Wpf;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class AdminFilmsViewModel : 
        UserControlViewModel, INotifyPropertyChanged
    {

        private DataView _films;

        public DataView Films
        {
            get
            {
                if (_films != null) return _films;
                _films = GetFilmsFromDb();
                return _films;
            }
            set
            {
                _films = value;
                OnPropertyChanged("Films");
            }
        }

        private RelayCommand _addFilm;
        public RelayCommand AddFilm => _addFilm ?? (_addFilm = new RelayCommand(async o =>
        {
            var addFilmDialog = new AddFilmDialogViewModel(_messenger, UpdateFilms);
            await DialogHost.Show(addFilmDialog,
                delegate(object sender, DialogOpenedEventArgs args)
                {
                    addFilmDialog.CloseAction = args.Session.Close;
                });
        }));

        private DataView GetFilmsFromDb()
        {
            DataTable dataTable = new DataTable();
            using (SQLiteConnection connection =
                new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter();
                adapter.SelectCommand =
                    new SQLiteCommand(@"SELECT f.name,
                                               f.year,
                                               f.rating,
                                               ifnull(c.phone, '') AS owner
                                          FROM film f
                                               LEFT JOIN
                                               client_film cf ON f.id = cf.film_id
                                               LEFT JOIN
                                               client c ON cf.client_id = c.id",
                        connection);
                adapter.Fill(dataTable);

                //Filling film genres
                /*dataTable.Columns.Add("genre");
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    //TODO make as a one query
                    var sql =
                        $"SELECT g1.name AS genre FROM genre g1" +
                        $" JOIN film_genre fg1 " +
                        $"ON g1.id==fg1.genre_id " +
                        $"JOIN film f1 " +
                        $"ON f1.id==fg1.film_id " +
                        $"WHERE f1.id=={i + 1}";//TODO

                    SQLiteCommand command = new SQLiteCommand(sql, connection);
                    SQLiteDataReader reader = command.ExecuteReader();

                    reader.Read();
                    string result = reader["genre"].ToString();

                    while (reader.Read())
                        result += ", " + reader["genre"];

                    dataTable.Rows[i]["genre"] = result;//TODO Tip for fix!
                }*/

                connection.Close();
                _films = dataTable.DefaultView;
                return _films;
            }
        }
        public void  DeleteFilm(string name, string year, string rating)
        {
            using (var con = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                con.Open();
                var command = new SQLiteCommand($"select id from film where name='{name}' and year='{year}' and rating={rating}", con);
                var reader = command.ExecuteReader();
                reader.Read();
                int filmId = int.Parse(reader["id"].ToString());

                ExecuteNonQuery($"delete from film where id={filmId}",con);
                ExecuteNonQuery($"delete from film_genre where film_id={filmId}",con);
                con.Close();
                UpdateFilms();
            }
        }

        private void UpdateFilms()
        {
            Films = GetFilmsFromDb();
        }

        private void ExecuteNonQuery(string query, SQLiteConnection connection)
        {
            var command = new SQLiteCommand(query, connection);
            command.ExecuteNonQuery();
        }

        public AdminFilmsViewModel(IMvxMessenger messenger) : base(messenger)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

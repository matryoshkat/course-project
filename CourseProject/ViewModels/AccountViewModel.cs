﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.Properties;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class AccountViewModel : 
        UserControlViewModel, INotifyPropertyChanged
    {

        public DataView Films
        {
            get
            {
                DataTable dt = new DataTable();
                using (SQLiteConnection connection =
                    new SQLiteConnection(Settings.Default.ConnectionString))
                {
                    connection.Open();
                    var adapter = new SQLiteDataAdapter();
                    adapter.SelectCommand = new SQLiteCommand($@"
                                                                SELECT film.name,
                                                                client_film.rent_start_date,
                                                                client_film.rent_end_date
                                                                    FROM client_film
                                                                    JOIN
                                                                client ON client_film.client_id = client.id
                                                                JOIN
                                                                    film ON client_film.film_id = film.id
                                                                WHERE client_film.client_id = {_userId};
                    ", connection);
                    adapter.Fill(dt);
                    connection.Close();
                    return dt.DefaultView;
                }
            }
        }

        private int _userId;

        private RelayCommand _openBrowsingCommand;
        public RelayCommand OpenBrowsingCommand =>
            _openBrowsingCommand ?? (_openBrowsingCommand = new RelayCommand(o =>
            {
                _messenger.Publish(new OpenBrowsingModeMessage(this, _userId));
            }));

        private RelayCommand _deauthCommand;

        public RelayCommand DeauthCommand => _deauthCommand ?? (_deauthCommand = new RelayCommand(o =>
        {
            _messenger.Publish(new OpenLoginViewMessage(this));
        }));


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public AccountViewModel(IMvxMessenger messenger, int userId) : base(messenger)
        {
            _userId = userId;
        }
    }
}

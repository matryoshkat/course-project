﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class AdminControlViewModel : 
        UserControlViewModel, INotifyPropertyChanged
    {

        private AdminClientsViewModel _adminClientsViewModel;
        private AdminFilmsViewModel _adminFilmsViewModel;
        private AdminGenresViewModel _adminGenresViewModel;

        private UserControlViewModel _currentView;

        public UserControlViewModel CurrentView
        {
            get => _currentView;
            set
            {
                _currentView = value;
                OnPropertyChanged("CurrentView");
            }
        }

        private RelayCommand _openLoginWidow;
        public RelayCommand OpenRelayCommand => _openLoginWidow ?? (_openLoginWidow = new RelayCommand(o =>
        {
            _messenger.Publish(new OpenLoginViewMessage(this));
        }));

        public void OpenClientsView()
        {
            if(CurrentView.GetType() == typeof(AdminClientsViewModel)) return;
            CurrentView = _adminClientsViewModel ?? (_adminClientsViewModel = new AdminClientsViewModel(_messenger));
        }

        public void OpenFilmsView()
        {
            if (CurrentView.GetType() == typeof(AdminFilmsViewModel)) return;
            CurrentView = _adminFilmsViewModel ?? (_adminFilmsViewModel = new AdminFilmsViewModel(_messenger));
        }

        public void OpenGenresView()
        {
            if (CurrentView.GetType() == typeof(AdminGenresViewModel)) return;
            CurrentView = _adminGenresViewModel ?? (_adminGenresViewModel = new AdminGenresViewModel(_messenger));
        }

        public AdminControlViewModel(IMvxMessenger messenger) : base(messenger)
        {
            CurrentView = (new AdminClientsViewModel(_messenger));
        }

        public void Reset()
        {
            _adminClientsViewModel = new AdminClientsViewModel(_messenger);
            CurrentView = _adminClientsViewModel;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }       
    }
}

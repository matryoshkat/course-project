﻿using System;
using System.ComponentModel;
using System.Data.SQLite;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using CourseProject.Helpers;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.Properties;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class LoginViewModel : 
        UserControlViewModel, INotifyPropertyChanged{
        public event PropertyChangedEventHandler PropertyChanged;


        private string _loginUI;
        public string LoginUI {
            get => _loginUI;
            set {
                _loginUI = value;
                OnPropertyChanged("LoginUI");
            }
        }

        private string _passUI;
        public string PassUI {
            get => _passUI;
            set {
                _passUI = value;
                OnPropertyChanged("PassUI");
            }
        }

        private RelayCommand _loginCommand;

        public RelayCommand LoginCommand =>
            _loginCommand ?? (_loginCommand = new RelayCommand(o =>
            {
                var passField = o as PasswordBox;
                PassUI = passField.Password;
                if(String.IsNullOrEmpty(PassUI)) return;
                if (LoginUI == "admin" && PassUI == "admin")
                {
                    _messenger.Publish(new OpenAdminControlMessage(this));
                    return;
                }

                using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
                {
                    connection.Open();

                    var passHash = AuthHelper.GetHashString(PassUI);

                    string sql =
                        $"select id from client where login='{LoginUI.ToLower()}' and pass='{passHash}'";

                    SQLiteCommand command = new SQLiteCommand(sql, connection);
                    var reader = command.ExecuteReader();
                    if (!reader.Read())
                    {
                        //Bad login
                        return;
                    }

                    int userId = int.Parse(reader["id"].ToString());
                    _messenger.Publish(new OpenAccountMessage(this, userId));
                }

            }));


        private RelayCommand _browsingCommand;

        public RelayCommand BrowsingCommand =>
            _browsingCommand ?? (_browsingCommand = new RelayCommand(o =>
            {
                _messenger.Publish(new OpenBrowsingModeMessage(this));
            }));


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public LoginViewModel(IMvxMessenger messenger) : base(messenger)
        {
        }
    }
}

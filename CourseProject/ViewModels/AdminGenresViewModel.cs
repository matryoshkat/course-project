﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Properties;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class AdminGenresViewModel : UserControlViewModel, INotifyPropertyChanged
    {
        private string _newGenreName { get; set; } = String.Empty;

        public string NewGenreName
        {
            get => _newGenreName;
            set
            {
                _newGenreName = value;
                OnPropertyChanged("NewGenreName");
            }
        }

        private DataView _genres;
        public DataView Genres
        {
            get => _genres ?? (_genres = GetGenresFromDb());
            set
            {
                _genres = value;
                OnPropertyChanged("Genres");
            }

        }

        private RelayCommand _addGenre;

        public RelayCommand AddGenre => _addGenre ?? (_addGenre = new RelayCommand(o =>
        {
            if (string.IsNullOrEmpty(NewGenreName)) return;
            using (SQLiteConnection connection =
                new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = $"insert into genre(name) values ('{NewGenreName}')";
                command.ExecuteNonQuery();
                connection.Close();
            }

            Genres = GetGenresFromDb();
            NewGenreName = String.Empty;
        }));

        private DataView GetGenresFromDb()
        {
            DataTable dt = new DataTable();
            using (SQLiteConnection connection =
                new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                var adapter = new SQLiteDataAdapter();
                adapter.SelectCommand = new SQLiteCommand($@"select * from genre", connection);
                adapter.Fill(dt);
                connection.Close();
                return dt.DefaultView;
            }
        }

        public AdminGenresViewModel(IMvxMessenger messenger) : base(messenger)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

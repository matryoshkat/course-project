﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.ViewModels.Dialogs;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels
{
    class StartWidowViewModel : INotifyPropertyChanged
    {
        private MvxSubscriptionToken _openAdminControlToken;
        private MvxSubscriptionToken _openBrowsingModeToken;
        private MvxSubscriptionToken _openAccountToken;
        private MvxSubscriptionToken _openLoginToken;
        private MvxSubscriptionToken _loginToken;
        private MvxMessengerHub _messenger;

        private int _userId;

        private UserControlViewModel _currentView;
        public UserControlViewModel CurrentView {
            get => _currentView;
            set {
                _currentView = value;
                OnPropertyChanged("CurrentView");
            }
        }

        public StartWidowViewModel() {
            _messenger = new MvxMessengerHub();
            _openAdminControlToken = _messenger.Subscribe<OpenAdminControlMessage>(OpenAdminView);
            _openBrowsingModeToken = _messenger.Subscribe<OpenBrowsingModeMessage>(OpenBrowsingMode);
            _openAccountToken = _messenger.Subscribe<OpenAccountMessage>(OpenAccount);
            _openLoginToken = _messenger.Subscribe<OpenLoginViewMessage>(OpenLogin);
            _loginToken = _messenger.Subscribe<LoginMessage>(SetUserId);

            CurrentView = new LoginViewModel(_messenger);
        }


        private void OpenAccount(OpenAccountMessage message)
        {
            CurrentView = new AccountViewModel(_messenger, message.UserId);
        }

        private void SetUserId(LoginMessage message)
        {
            _userId = message.UserId;
        }

        private void OpenLogin(OpenLoginViewMessage message)
        {
            _userId = 0;
            CurrentView = new LoginViewModel(_messenger);
        }

        private void OpenAdminView(OpenAdminControlMessage openAdminControlMessage)
        {
            CurrentView = new AdminControlViewModel(_messenger);
        }

        private void OpenBrowsingMode(OpenBrowsingModeMessage openBrowsingModeMessage)
        {
            if (_userId == 0)
                _userId = openBrowsingModeMessage.UserId;
            CurrentView = new BrowseModeViewModel(_messenger, _userId);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class FilmSearchDialogViewModel : UserControlViewModel
    {

        private RelayCommand _searchFilmCommand;

        public RelayCommand SearchFilmCommand => _searchFilmCommand ?? (_searchFilmCommand = new RelayCommand(o =>
        {
            _messenger.Publish(new SearchFilmMessage(this, SearchFilm));
            CloseAction();
        }));

        public Action CloseAction { get; set; }

        public string SearchFilm { get; set; }

        public FilmSearchDialogViewModel(IMvxMessenger messenger) : base(messenger)
        {
        }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SQLite;
using System.Globalization;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Properties;
using MaterialDesignThemes.Wpf;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class AddFilmDialogViewModel :
        UserControlViewModel, INotifyPropertyChanged
    {
        public string FilmName { get; set; }
        public string FilmYear { get; set; }
        public string FilmRating { get; set; }
        public ObservableCollection<Genre> GenresList { get; set; } = new ObservableCollection<Genre>();
        private Action UpdateFilmsAction;

        private RelayCommand _addFilm;
        public RelayCommand AddFilm => _addFilm ?? (_addFilm = new RelayCommand(o =>
        {
            if(string.IsNullOrEmpty(FilmName) || string.IsNullOrEmpty(FilmYear) ||
               string.IsNullOrEmpty(FilmRating)) return;

            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();

                ExecuteNonQuery($"insert into film(name, year, rating, available) values ('{FilmName}', '{FilmYear}', " +
                                $"'{FilmRating.ToString(new CultureInfo("en-US"))}', 'true')", connection);

                var command = new SQLiteCommand("select max(id) from film", connection);
                var reader = command.ExecuteReader();
                reader.Read();
                int filmId = int.Parse(reader["max(id)"].ToString());

                foreach (var genre in GenresList)
                {
                    if (genre.IsSelected)
                    {
                        ExecuteNonQuery($"insert into film_genre (film_id, genre_id) values ({filmId},{genre.Id})",
                            connection);
                    }
                }

                UpdateFilmsAction();
                CloseAction();
                connection.Close();
            }

        }));

        private RelayCommand _close;
        public RelayCommand Close => _close ?? (_close = new RelayCommand(o => CloseAction()));

        private RelayCommand _openGenresList;
        public RelayCommand OpenGenresList => _openGenresList ?? (_openGenresList = new RelayCommand(async o =>
        {
            var genresSelectDialog = new GenresSelectDialogViewModel(_messenger, GenresList);
            await DialogHost.Show(genresSelectDialog, "AddFilmDialogHost", delegate (object sender, DialogOpenedEventArgs args)
            {
                genresSelectDialog.CloseAction = args.Session.Close;
            });
            OnPropertyChanged("GenresList");
        }));

        public AddFilmDialogViewModel(IMvxMessenger messenger, Action updateFilmAction) : base(messenger)
        {
            FillGenreList();
            UpdateFilmsAction = updateFilmAction;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Action CloseAction { get; set; }

        private void FillGenreList()
        {
            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                var sql = "select * from genre";
                var command = new SQLiteCommand(sql, connection);
                var reader = command.ExecuteReader();

                int i = 1;
                while (reader.Read())
                {
                    
                    var genre = new Genre { Name = reader["name"].ToString(), IsSelected = false };
                    genre.Id = i++; 
                    GenresList.Add(genre);
                }
            }
        }
        private void ExecuteNonQuery(string query, SQLiteConnection connection)
        {
            var command = new SQLiteCommand(query, connection);
            command.ExecuteNonQuery();
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

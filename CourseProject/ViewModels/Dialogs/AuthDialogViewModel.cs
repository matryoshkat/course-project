﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CourseProject.Helpers;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using CourseProject.Properties;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class AuthDialogViewModel :
        UserControlViewModel, INotifyPropertyChanged
    {
        public Action CloseAction { get; set; }

        private bool _isErrorAppear;

        public bool IsErrorAppear {
            get => _isErrorAppear;
            set {
                _isErrorAppear = value;
                OnPropertyChanged("IsErrorAppear");
            }
        }


        private string _errorMessage;

        public string ErrorMessage {
            get => _errorMessage;
            set {
                _errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        private string _authLogin;
        public string AuthLogin
        {
            set
            {
                _authLogin = value;
                OnPropertyChanged("AuthLogin");
            }
        }

        private string _authPass;

        public string AuthPass
        {
            set
            {
                _authPass = value;
                OnPropertyChanged("AuthPass");
            }
        }

        private string _regName;

        public string RegName {
            get => _regName;
            set
            {
                _regName = value;
                OnPropertyChanged("RegName");
            }
        }

        private string _regPhone;

        public string RegPhone {
            get => _regPhone;
            set
            {
                _regPhone = value;
                OnPropertyChanged("RegPhone");
            }
        }

        private string _regLogin;

        public string RegLogin {
            get => _regLogin;
            set
            {
                _regLogin = value;
                OnPropertyChanged("RegLogin");
            }
        }

        private string _regPass;

        public string RegPass {
            get => _regPass;
            set
            {
                _regPass = value;
                OnPropertyChanged("RegPass");
            }
        }

        private RelayCommand _regCommand;
        public RelayCommand RegCommand => _regCommand ?? (_regCommand = new RelayCommand(o =>
        {
            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();
                //SQL Injection alert! ><
                //check login
                if (CheckEmptyProperties()) {
                    IsErrorAppear = true;
                    return;
                }
                var sql = $"select login from client where login='{_regLogin}'";
                var command = new SQLiteCommand(sql, connection);
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    //login already exists
                }
                reader.Close();
                var sql2 = $"select phone from client where phone='{_regPhone}'";
                var command2 = new SQLiteCommand(sql, connection);
                var reader2 = command2.ExecuteReader();
                if (reader2.Read())
                {
                    //phone already exists
                }
                reader2.Close();

                //check phone lenght

                var passHash = AuthHelper.GetHashString(_regPass);

                var addSql =
                    $"insert into client (name, phone, login, pass) values ('{_regName}', '{_regPhone}', '{_regLogin.ToLower()}', '{passHash}')";

                NonQueryDbCommand(addSql, connection);
                //get newly added userId for future
                string getIdSql =
                    $"select id from client where login='{_regLogin.ToLower()}'";
                SQLiteCommand getIdCommand = new SQLiteCommand(getIdSql, connection);
                var idReader = getIdCommand.ExecuteReader();
                idReader.Read();
                int userId = int.Parse(idReader["id"].ToString());

                _messenger.Publish(new LoginMessage(this, userId));
                if (_direction == dir.ToAccount)
                    _messenger.Publish(new OpenAccountMessage(this, userId));
                CloseAction();
            }
        }));

        private RelayCommand _snackOkCommand;

        public RelayCommand SnackOkCommand =>
            _snackOkCommand ?? (_snackOkCommand = new RelayCommand(o => {
                IsErrorAppear = false;
                ErrorMessage = String.Empty;
            }));

        private bool CheckEmptyProperties() {
            if (string.IsNullOrEmpty(RegName)) {
                ErrorMessage = "Ведiть iм'я";
                return true;
            }
            if (string.IsNullOrEmpty(RegPhone)) {
                ErrorMessage = "Ведiть номер телефону";
                return true;
            }
            if (string.IsNullOrEmpty(RegLogin)) {
                ErrorMessage = "Ведiть логiн";
                return true;
            }
            if (string.IsNullOrEmpty(RegPass)) {
                ErrorMessage = "Ведiть пароль";
                return true;
            }
            return false;
        }

        private RelayCommand _loginCommand;

        public RelayCommand LoginCommand => _loginCommand ?? (_loginCommand = new RelayCommand(o =>
        {
            using (var connection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                connection.Open();

                var passHash = AuthHelper.GetHashString(_authPass);

                string sql =
                    $"select id from client where login='{_authLogin.ToLower()}' and pass='{passHash}'";

                SQLiteCommand command = new SQLiteCommand(sql, connection);
                var reader = command.ExecuteReader();
                if (!reader.Read())
                {
                    //Bad login
                    return;
                }

                int userId = int.Parse(reader["id"].ToString());
                _messenger.Publish(new LoginMessage(this, userId));
                if (_direction == dir.ToAccount)
                    _messenger.Publish(new OpenAccountMessage(this, userId));
                CloseAction();
            }
        }));

        private RelayCommand _close;
        public RelayCommand Close => _close ?? (_close = new RelayCommand(o => CloseAction()));

        private void NonQueryDbCommand(string sql, SQLiteConnection connection)
        {
            var command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
        }


        public AuthDialogViewModel(IMvxMessenger messenger, dir direction = dir.Default) : base(messenger)
        {
            _direction = direction;
        }

        private dir _direction;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

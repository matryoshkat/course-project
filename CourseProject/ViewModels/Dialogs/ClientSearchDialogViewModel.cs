﻿using System;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class ClientSearchDialogViewModel : UserControlViewModel
    {
        public string PhoneSearch{ get; set; } = String.Empty;

        public Action CloseAction { get; set; }

        private RelayCommand _searchCommand;
        public RelayCommand SearchCommand => _searchCommand ?? (_searchCommand = new RelayCommand(o =>
                                                 {
                                                     _messenger.Publish(new SearchClientMessage(this, PhoneSearch));
                                                     CloseAction();
                                                 }));

        public ClientSearchDialogViewModel(IMvxMessenger messenger) : base(messenger)
        {
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Data.SQLite;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Helpers.Base.Messages;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class EditClientDialogViewModel 
     :UserControlViewModel, INotifyPropertyChanged
    {
        private string _loginOrigin;

        private string _login;
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged("Login");
            }
        }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _phone;
        public string Phone
        {
            get => _phone;
            set
            {
                _phone = value;
                OnPropertyChanged("Phone");
            }
        }

        private string _pass;
        public string Pass
        {
            get => _pass;
            set
            {
                _pass = value;
                OnPropertyChanged("Pass");
            }
        }

        private RelayCommand _confirmEditCommand;

        public RelayCommand ConfirmEditCommand => _confirmEditCommand ?? (_confirmEditCommand = new RelayCommand(o =>
        {
            using (var connection = new SQLiteConnection(Properties.Settings.Default.ConnectionString))
            {
                connection.Open();

                var query =
                    $@"update client SET name='{Name}', phone={Phone}, login='{Login}', pass='{Pass}' WHERE login='{
                            _loginOrigin
                        }'";

                var command = new SQLiteCommand(query, connection);
                command.ExecuteNonQuery();
                connection.Close();
                _messenger.Publish(new UpdateClientsMessage(this));
            }
        }));

        public EditClientDialogViewModel(IMvxMessenger messenger, string login) : base(messenger)
        {
            Login = login;
            _loginOrigin = login;
            using (var connetion = new SQLiteConnection(Properties.Settings.Default.ConnectionString))
            {
                connetion.Open();

                var query = $@"SELECT name,
                                   phone,
                                   pass
                              FROM client
                             WHERE login = '{Login}'";

                var command = new SQLiteCommand(query, connetion);
                var reader = command.ExecuteReader();

                reader.Read();
                Name = reader["name"].ToString();
                Phone = reader["phone"].ToString();
                Pass = reader["pass"].ToString();

                connetion.Close();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Action CloseAction { get; set; }
    }
}

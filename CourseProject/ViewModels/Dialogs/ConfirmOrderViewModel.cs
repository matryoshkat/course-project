﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Globalization;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using CourseProject.Properties;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class ConfirmOrderDialogViewModel :
        UserControlViewModel, INotifyPropertyChanged
    {

        public Action CloseAction { get; set; }

        private string _startDate;
        public string StartDate
        {
            get => _startDate;
            set
            {
                _startDate = value;
                OnPropertyChanged("StartDate");
            }
        }

        private string _endDate;
        public string EndDate
        {
            get => _endDate;
            set
            {
                _endDate = value;
                OnPropertyChanged("EndDate");
            }
        }


        private RelayCommand _confirmAndCloseCommand;
        public RelayCommand ConfirmAndCloseCommand =>
            _confirmAndCloseCommand ?? (_confirmAndCloseCommand = new RelayCommand(
                o =>
                {
                    using (SQLiteConnection connection = new SQLiteConnection(Settings.Default.ConnectionString))
                    {
                        connection.Open();
                        SQLiteTransaction sqlTransaction = connection.BeginTransaction();

                        foreach (var film in _filmList)
                        {
                            string sql =
                                $"select id from film where name='{film.Name}' and year='{film.Year}' and rating={film.Rating.ToString(new CultureInfo("en-US"))}";
                            var selectCommand = new SQLiteCommand(sql, connection);
                            var reader = selectCommand.ExecuteReader();
                            reader.Read();
                            film.Id = reader["id"].ToString();

                            string insertSql =
                                $"insert into client_film(client_id, film_id, rent_start_date, rent_end_date) values ({_userId}, {int.Parse(film.Id)}, " +
                                $"'{DateTime.Today:d}', '{DateTime.Today.AddDays(7):d}')";
                            var command = connection.CreateCommand();
                            command.Transaction = sqlTransaction;

                            command.CommandText = insertSql;
                            command.ExecuteNonQuery();
                            command.CommandText = $"update film SET available='false' WHERE id={int.Parse(film.Id)}";
                            command.ExecuteNonQuery();

                        }
                        sqlTransaction.Commit();
                        connection.Close();
                    }
                    CloseAction();
                }));

        private RelayCommand _close;
        public RelayCommand Close => _close ?? (_close = new RelayCommand(o => CloseAction()));

        public event PropertyChangedEventHandler PropertyChanged;

        private int _userId;
        private List<Film> _filmList;

        private void NonQueryDbCommand(string sql, SQLiteConnection connection)
        {
            var command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ConfirmOrderDialogViewModel(IMvxMessenger messenger, int userId, List<Film> filmList) : base(messenger)
        {
            _userId = userId;
            _filmList = filmList;
            InitDates();
        }

        private void InitDates()
        {
            StartDate  = DateTime.Today.ToString("d");
            EndDate = DateTime.Today.AddDays(7).ToString("d");
        }
    }
}

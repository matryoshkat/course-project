﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CourseProject.Helpers.Base;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.ViewModels.Dialogs
{
    class GenresSelectDialogViewModel : 
        UserControlViewModel,INotifyPropertyChanged
    {

        private ObservableCollection<Genre> _searchGenreList;

        public ObservableCollection<Genre> SearchGenreList
        {
            get => _searchGenreList;
            set
            {
                _searchGenreList = value;
                OnPropertyChanged("SearchGenreList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public GenresSelectDialogViewModel(IMvxMessenger messenger, ObservableCollection<Genre> list) : base(messenger)
        {
            SearchGenreList = list;
        }

        public Action CloseAction { get; set; }
    }
}

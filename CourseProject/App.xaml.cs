﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Windows;
using CourseProject.Helpers;
using CourseProject.Properties;
using CourseProject.Views;
using Microsoft.Win32;

namespace CourseProject
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private const int COUNT_OF_RANDOM_FILMS = 100;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var startWidow = new StartWidowView();
            Init();
            startWidow.Show();
        }

        private void Init()
        {

            //check db existing
            if (string.IsNullOrEmpty(Settings.Default.ConnectionString) || Settings.Default.ConnectionString == "Data Source=;Version=3;")
                PathToDb.ChangePathToDb();    

            //Check last time backup 
            if (DateTime.Now > Settings.Default.LastTimeBackup.AddDays(7))
            {
                BackupHelper.BackupDbNow();
                Settings.Default.LastTimeBackup = DateTime.Now;
            }

            //Check last time archive 
            if (DateTime.Now > Settings.Default.LastTimeArchive.AddMonths(1))
            {
                BackupHelper.ArchiveDbNow();
                Settings.Default.LastTimeArchive = DateTime.Now;
            }

            if (Settings.Default.IsFirstLaunch)
            {
                FirstPreparation();
            }
            else
            {
                if(File.Exists("video-shop.sqlite"))
                    return;
                FirstPreparation();
            }
                
        }

        private void FirstPreparation()
        {
            var firstLaunchWidow = new FirstLaunchWidow();
            firstLaunchWidow.Show();
            FirstLaunch();
            //FillFilmTable();
            firstLaunchWidow.Close();
        }

        private void FillFilmTable()
        {
            using (var dbConntection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                dbConntection.Open();
                for (int i = 0; i < COUNT_OF_RANDOM_FILMS; i++)
                {
                    string addFilm = $"insert into film (name,year,rating,available) " +
                                         $"values (" +
                                     $"'{FilmGenerator.getInstance().GetFilmName()}'," +
                                     $"'{FilmGenerator.getInstance().GetYear()}'," +
                                     $"'{FilmGenerator.getInstance().GetRating()}'," +
                                     $"'true')";

                    var genres = FilmGenerator.getInstance().GetGenresId();
                    
                    for (int j = 0; j <genres.Length ; j++)
                    {
                        string addGenre = $"insert into film_genre(film_id, genre_id) values (" +
                                          $"'{i + 1}', '{genres[j]}')";
                        var addGenreCommand  = new SQLiteCommand(addGenre, dbConntection);
                        addGenreCommand.ExecuteNonQuery();
                    }

                    var addFilmCommand = new SQLiteCommand(addFilm, dbConntection);
                    addFilmCommand.ExecuteNonQuery();
                }
            }
        }

        private void FirstLaunch()
        {
            SQLiteConnection.CreateFile("video-shop.sqlite");
            using (var dbConntection = new SQLiteConnection(Settings.Default.ConnectionString))
            {
                dbConntection.Open();
               /* string sql = "create table genre (" +
                             "id int primary key not null," +
                             "name varchar(50) not null," +*/

                

                string createGenreTable = "create table if not exists genre(" +
                                          "id integer primary key autoincrement not null," +
                                          "name varchar(20) not null)";

                string createClientTable = "create table if not exists client(" +
                                           "id integer primary key autoincrement not null," +
                                           "name varchar(50) not null," +
                                           "phone varchar(10) not null," +
                                           "login varchar(50) not null ," +
                                           "pass varchar(100) not null unique)";


                string createFilmTable = "create table if not exists film (" +
                                         "id integer primary key autoincrement not null," +
                                         "name varchar(50) not null," +
                                         "year varchar(50) not null," +
                                         //                             "genre int not null," +
                                         "rating decimal," +
                                         "available bool not null)";
                                         

                string createFilm_GenreTable = "create table if not exists film_genre(" +
                                               "film_id integer references film (id) on delete cascade," +
                                               "genre_id integer references genre (id) on delete cascade" +
                                               ")";

                string createClient_FilmTable = @"CREATE TABLE client_film (
                                                    client_id INTEGER      REFERENCES client(id),
                                                    film_id INTEGER      REFERENCES film(id),
                                                    rent_start_date VARCHAR(50),
                                                    rent_end_date VARCHAR(50) 
                                                )";


                var command = new SQLiteCommand(createGenreTable, dbConntection);
                var command2 = new SQLiteCommand(createClientTable, dbConntection);
                var command3 = new SQLiteCommand(createFilmTable, dbConntection);
                var command4 = new SQLiteCommand(createFilm_GenreTable, dbConntection);
                var command5 = new SQLiteCommand(createClient_FilmTable, dbConntection);

                command.ExecuteNonQuery();
                command2.ExecuteNonQuery();
                command3.ExecuteNonQuery();
                command4.ExecuteNonQuery();
                command5.ExecuteNonQuery();

                string fillgenres = "insert into genre (name) values ('Вестерн'), " +
                                    "('Детектив'), ('Драма'), ('Екшн'), ('Історичний'), " +
                                    "('Комедія'), ('Науковий'), ('Художній')";

                var command6 = new SQLiteCommand(fillgenres, dbConntection);

                command6.ExecuteNonQuery();
            }
            Settings.Default.IsFirstLaunch = false;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            Settings.Default.Save();
        }
    }
}

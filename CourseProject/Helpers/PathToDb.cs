﻿using System;
using CourseProject.Properties;
using Microsoft.Win32;

namespace CourseProject.Helpers
{
    public static class PathToDb
    {
        public static void ChangePathToDb()
        {
            string pathToFile = String.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                pathToFile = openFileDialog.FileName;
            Settings.Default.ConnectionString = $"Data Source={pathToFile};Version=3;";
        }
    }
}

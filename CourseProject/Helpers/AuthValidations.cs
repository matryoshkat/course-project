﻿using System.Globalization;
using System.Windows.Controls;

namespace CourseProject.Helpers {
    class RegPhoneValid : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {

            string input = (value ?? "").ToString();
            if(input=="")
                return ValidationResult.ValidResult;
            if (input.Length != 10) return new ValidationResult(false, "не правильний формат номеру");
            return ValidationResult.ValidResult;

        }
    }

    class RegLoginValid : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            return ValidationResult.ValidResult;
        }
    }
}

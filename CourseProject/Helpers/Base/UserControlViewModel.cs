﻿using System;
using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base
{
    public abstract class UserControlViewModel
    {
        protected IMvxMessenger _messenger; 

        protected UserControlViewModel(IMvxMessenger messenger)
        {
            _messenger = messenger ?? throw new ArgumentNullException(nameof(messenger));
        }
    }
}

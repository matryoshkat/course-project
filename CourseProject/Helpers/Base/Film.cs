﻿namespace CourseProject.Helpers.Base
{
    class Film
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Year { get; set; }

        public double Rating { get; set; }

        public bool IsAvailable { get; set; }

        public int OwnerId { get; set; }

        public string Genres { get; set; }
    }
}

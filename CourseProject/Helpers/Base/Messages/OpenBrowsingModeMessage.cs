﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class OpenBrowsingModeMessage : MvxMessage
    {
        public int UserId { get; set; }

        public OpenBrowsingModeMessage(object sender, int userId = 0) : base(sender)
        {
            UserId = userId;
        }
    }
}

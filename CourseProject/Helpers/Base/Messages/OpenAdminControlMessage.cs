﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class OpenAdminControlMessage :
        MvxMessage
    {
        public OpenAdminControlMessage(object sender) : base(sender)
        {

        }
    }
}

﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    enum dir
    {
        Default,
        ToAccount
    }
    class OpenAuthAndRegMessage : MvxMessage
    {

        public dir Direction { get; set; }
        public OpenAuthAndRegMessage(object sender, dir dir = dir.Default) : base(sender)
        {
            Direction = dir;
        }
    }
}

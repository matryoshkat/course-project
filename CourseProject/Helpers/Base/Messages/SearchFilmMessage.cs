﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class SearchFilmMessage : MvxMessage
    {
        public string FilmName { get; set; }
        public SearchFilmMessage(object sender, string name) : base(sender)
        {
            FilmName = name;
        }
    }
}

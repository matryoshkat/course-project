﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class OpenAccountMessage : MvxMessage
    {
        public int UserId { get; set; }
        public OpenAccountMessage(object sender, int userId) : base(sender)
        {
            UserId = userId;
        }
    }
}

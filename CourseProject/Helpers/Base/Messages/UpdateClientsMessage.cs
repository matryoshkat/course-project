﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class UpdateClientsMessage : MvxMessage
    {
        public UpdateClientsMessage(object sender) : base(sender)
        {
        }
    }
}

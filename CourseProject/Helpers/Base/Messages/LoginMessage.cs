﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class LoginMessage : MvxMessage
    {
        public int UserId { get; }
        
        public LoginMessage(object sender, int userId) : base(sender)
        {
            UserId = userId;
        }
    }
}

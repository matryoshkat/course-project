﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class OpenLoginViewMessage : MvxMessage
    {
        public OpenLoginViewMessage(object sender) : base(sender)
        {
        }
    }
}

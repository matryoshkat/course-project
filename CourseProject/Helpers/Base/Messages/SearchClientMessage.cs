﻿using MvvmCross.Plugins.Messenger;

namespace CourseProject.Helpers.Base.Messages
{
    class SearchClientMessage : MvxMessage
    {
        public string Phone { get; set; }
        public SearchClientMessage(object sender, string phone) : base(sender)
        {
            Phone = phone;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject.Helpers
{
    class FilmGenerator
    {
        private const int GENRES_COUNT = 8;
        private static FilmGenerator instance;

        private List<string> _nouns;
        private List<string> _objects;
        private List<string> _possessiveAdjectives;
        private Random _random;

        private FilmGenerator()
        {
            _nouns = new List<string>();
            _objects = new List<string>();
            _possessiveAdjectives = new List<string>();
            _random = new Random();

            try
            {
                using (var reader = new StreamReader("generator/var1/nouns.txt", Encoding.GetEncoding(1251)))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null) 
                        _nouns.Add(line);
                }

                using (var reader = new StreamReader("generator/var1/objects.txt", Encoding.GetEncoding(1251)))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        _objects.Add(line);
                }

                using (var reader = new StreamReader("generator/var1/possessive adjectives.txt", Encoding.GetEncoding(1251)))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                        _possessiveAdjectives.Add(line);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GetFilmName()
        {
            string resultString;
            resultString = _nouns[_random.Next(0, _nouns.Count - 1)] + " ";
            resultString += _possessiveAdjectives[_random.Next(0, _possessiveAdjectives.Count - 1)] + " ";
            resultString += _objects[_random.Next(0, _objects.Count - 1)];

            return resultString;
        }

        public int GetYear()
        {
            return _random.Next(1960, 2018);
        }

        public int[] GetGenresId()
        {
            int[] result;
            var countOfGenres = _random.Next(1, 4);
            result = new int[countOfGenres];
            List<int> helpList = new List<int>();
            for (int i = 1; i <= GENRES_COUNT; i++)
            {
                helpList.Add(i);
            }

            for (int i = 0; i < countOfGenres; i++)
            {
                var randomValue = _random.Next(0, helpList.Count - 1);
                result[i] = helpList[randomValue];
                helpList.Remove(helpList[randomValue]);
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Its necessary to zero check</returns>
        public string GetRating()
        {
            var value = _random.Next(0, 100);
            if (value >= 0 && value <= 80)
            {
                string stringResult;
                var wholeNumber = _random.Next(5, 8);
                var fractionNumber = _random.Next(0, 9);
                stringResult = wholeNumber + "." + fractionNumber;
                return stringResult;

            }

            if (value > 80 && value <=90)
            {
                string stringResult;
                var wholeNumber = 9;
                var fractionNumber = _random.Next(0, 9);
                stringResult = wholeNumber + "." + fractionNumber;
                return stringResult;
            }

            if (value > 90)
            {
                string stringResult;
                var wholeNumber = _random.Next(2, 4);
                var fractionNumber = _random.Next(0, 9);
                stringResult = wholeNumber + "." + fractionNumber;
                return stringResult;
            }

            throw null;
        }

        public static FilmGenerator getInstance()
        {
            return instance ?? (instance = new FilmGenerator());
        }


    }
}

﻿using System;
using System.Security.Cryptography;
using System.Text;
using CourseProject.Properties;

namespace CourseProject.Helpers
{
    static class AuthHelper
    {
        public static string GetHashString(string inputString)
        {
            var salt = Settings.Default.DefaultSalt;

            var data = Encoding.ASCII.GetBytes(inputString + salt);

            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(data);

            string result = String.Empty;

            foreach (byte b in md5data)
            {
                result += b + " ";
            }

            return result;
        }

        public static bool CheckPassword(string checkedkPass, string presentHash)
        {
            return presentHash == GetHashString(checkedkPass);
        }
    }
}

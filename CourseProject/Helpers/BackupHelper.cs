﻿using System;

namespace CourseProject.Helpers
{
    public static class BackupHelper
    {
        public static void BackupDbNow()
        {
            if (!System.IO.Directory.Exists("backup"))
            {
                System.IO.Directory.CreateDirectory("backup");
            }

            System.IO.File.Copy("video-shop.sqlite", "backup/video-shop.sqlite", true);
        }

        public static void ArchiveDbNow()
        {
            if (!System.IO.Directory.Exists("archive"))
            {
                System.IO.Directory.CreateDirectory("archive");
            }

            System.IO.File.Copy("video-shop.sqlite", $"archive/video-shop-{DateTime.Today:d-M-yyyy}.sqlite", true);
        }
    }
}
